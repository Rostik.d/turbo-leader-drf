from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from applications.utils.utils import get_object_or_none
from .models import User
from applications.contact.models import Contact


class UsersCreateSerializer(serializers.ModelSerializer):
    username = serializers.SlugField(
        min_length=4,
        max_length=500,
        help_text=_(
            'Required. 4-32 characters. Letters, numbers, underscores or hyphens only.'
        ),
        validators=[UniqueValidator(
            queryset=User.objects.all(),
            message='has already been taken by other user'
        )],
        required=True
    )

    password = serializers.CharField(
        min_length=4,
        max_length=32,
        write_only=True,
        help_text=_(
            'Required. 4-32 characters.'
        ),
        required=True
    )

    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(
            queryset=User.objects.all(),
            message='has already been taken by other user'
        )]
    )
    first_name = serializers.CharField(
        required=True,
        max_length=32
    )
    last_name = serializers.CharField(
        required=True,
        max_length=32
    )

    gender = serializers.ChoiceField(
        choices=['Man', 'Woman']
    )

    class Meta:
        model = User
        fields = (
            'username',
            'password',
            'email',
            'first_name',
            'last_name',
            'gender',
        )

    def create(self, validated_data):
        request_object = self.context['request']
        password = validated_data.pop('password')
        ref_login = request_object.query_params.get('ref')

        user = User(**validated_data)
        user.set_password(password)
        user.save()

        referrals = get_object_or_none(User, username=ref_login)
        if referrals:
            Contact.objects.create(
                owner=referrals,
                user=user
            )
            User.objects.filter(id=user.id).update(
                referrals=referrals.username
            )
        else:
            admin = get_object_or_none(User, username="admin")
            Contact.objects.create(
                owner=admin,
                user=user
            )

        return user


class UserDefaultLogin(serializers.ModelSerializer):
    email = serializers.CharField(
        max_length=250,
        write_only=True,
        required=True,
    )
    password = serializers.CharField(
        min_length=4,
        max_length=32,
        write_only=True,
        required=True
    )
    token = serializers.CharField(allow_blank=True, read_only=True)

    class Meta:
        model = User
        fields = [
            'email',
            'password',
            'token',
        ]


class UserTokenSerializer(serializers.Serializer):
    email = serializers.CharField(label=_("Email"))
    password = serializers.CharField(
        label=_("Password"),
        style={'input_type': 'password'},
        trim_whitespace=False
    )

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        if email and password:
            user = authenticate(request=self.context.get('request'),
                                email=email, password=password)

            if not user:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = _('Must include "username" and "password".')
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs


class UserDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        exclude = [
            'password',
        ]


class UserUpdateSerializer(serializers.ModelSerializer):
    username = serializers.CharField(
        max_length=255,
        default='',
        validators=[UniqueValidator(
            queryset=User.objects.all(),
            message='has already been taken by other user'
        )]
    )
    email = serializers.EmailField(
        allow_blank=True,
        default='',
        validators=[UniqueValidator(
            queryset=User.objects.all(),
            message='has already been taken by other user'
        )]
    )

    class Meta:
        model = User
        fields = "__all__"


