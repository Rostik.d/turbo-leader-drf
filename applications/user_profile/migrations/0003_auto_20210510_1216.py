# Generated by Django 3.1.5 on 2021-05-10 09:16

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_profile', '0002_auto_20210508_0022'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='end_time',
            field=models.DateTimeField(default=datetime.datetime(2021, 5, 10, 12, 16, 0, 367534), null=True, verbose_name='Закінчення підписки'),
        ),
    ]
