from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from rest_framework import routers

from rest_framework_simplejwt.views import TokenRefreshView, TokenObtainPairView

from .views import (
    UserCreateAPIView,
    UserDefaultLoginView,
    UserLogoutAPIView,
    UserDetailAPIView,
    UserUpdateAPIView,
    PayUserCallBack,
    UserDetail,
    UserVerifySubs,
)

router = routers.SimpleRouter()
router.register("edit", UserUpdateAPIView, basename="user-update")

urlpatterns = [
    path('register/', UserCreateAPIView.as_view(), name='user-register'),
    path('login/', UserDefaultLoginView.as_view(), name='user-login'),
    path('logout/', UserLogoutAPIView.as_view(), name='user-logout'),
    path('profile/<str:username>/', UserDetailAPIView.as_view(), name='user-detail'),

    path('pay/', PayUserCallBack.as_view(), name='user-pay'),
    path('verify/', UserVerifySubs.as_view(), name='user-verify'),

    path('detail/', UserDetail.as_view(), name='user-request-detail'),

    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]

urlpatterns += router.urls

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

