import datetime

from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models

from rest_framework.authtoken.models import Token

from applications.utils.models import UUIDModel, TimeStamp


class CustomUserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, password, **extra_fields):
        user = self.model(**extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, password, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_active', True)
        return self._create_user(password, **extra_fields)

    def create_superuser(self, password, **extra_fields):
        extra_fields.setdefault('is_active', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_staff', True)
        if extra_fields.get('is_superuser') and extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_superuser=True and is_staff=True.')
        return self._create_user(password, **extra_fields)


class Group(models.Model):
    name = models.CharField('Group name', max_length=125, unique=True)

    objects = models.Manager()

    class Meta:
        verbose_name = 'Група'
        verbose_name_plural = 'Групи'

    def str(self):
        return self.name


AUTH_PROVIDERS = {'facebook': 'facebook', 'google': 'google',
                  'email': 'email'}


class User(UUIDModel, AbstractBaseUser, PermissionsMixin, TimeStamp):
    class Gender(models.TextChoices):
        MAN = 'Man'
        WOMAN = 'Woman'

    username = models.CharField(max_length=255, unique=True)
    first_name = models.CharField(max_length=100, null=True, blank=True)
    last_name = models.CharField(max_length=100, null=True, blank=True)
    email = models.EmailField(unique=True)
    gender = models.CharField(choices=Gender.choices, max_length=10)
    bio = models.TextField(max_length=2000, blank=True, default='', verbose_name='Biography')
    photo = models.ImageField(upload_to='profile_pic', default='default.jpg', blank=True)
    vk_link = models.URLField(verbose_name='Link VK')
    fb_link = models.URLField(verbose_name='Link Facebook')
    inst_link = models.URLField(verbose_name='Link Instagram')
    tg_link = models.CharField(max_length=100, verbose_name='Link Telegram')
    wt_link = models.CharField(max_length=100, verbose_name='Link WhatsApp')
    referral_link = models.URLField(blank=True, null=True, verbose_name='Referral Link x100')
    referrals = models.CharField(default='admin', max_length=200)
    is_paid = models.BooleanField(default=False)
    end_time = models.DateTimeField(
        default=datetime.datetime.now(), null=True, verbose_name='Закінчення підписки'
    )

    group_id = models.ForeignKey(Group, on_delete=models.DO_NOTHING, related_name='group', blank=True, null=True)

    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    auth_provider = models.CharField(
        max_length=255, blank=False, null=False, default=AUTH_PROVIDERS.get('email')
    )

    last_login = None
    groups = None
    user_permissions = None

    USERNAME_FIELD = 'email'

    objects = CustomUserManager()

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)
        return self

    def __str__(self):
        return f'{self.username} - {self.email} - Реферал({self.referrals}) - {self.created_ad}'

    class Meta:
        verbose_name = "Користувач"
        verbose_name_plural = "Користувачі"
