import datetime

import pytz
from dateutil.relativedelta import relativedelta
from rest_framework import generics, status
from rest_framework.authtoken.models import Token
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
)
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from .models import User
from .serializers import (
    UsersCreateSerializer,
    UserDefaultLogin,
    UserTokenSerializer,
    UserDetailSerializer,
    UserUpdateSerializer,
)


class UserCreateAPIView(generics.CreateAPIView):
    serializer_class = UsersCreateSerializer
    queryset = User.objects.all()
    permission_classes = [AllowAny]
    throttle_scope = 'create_user'


class UserDefaultLoginView(APIView):
    serializer_class = UserDefaultLogin
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        serializer = UserTokenSerializer(
            data=request.data,
            context={'request': request}
        )

        if serializer.is_valid(raise_exception=True):
            user = serializer.validated_data['user']
            token, created = Token.objects.get_or_create(user=user)
            return Response({
                'token': token.key,
                'username': user.username,
                'email': user.email,
                'first_name': user.first_name,
                'last_name': user.last_name,
                'is_active': user.is_active,
                'is_superuser': user.is_superuser,
                'is_staff': user.is_staff,
                'gender': user.gender,
            }, status=HTTP_200_OK)

        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class UserDetailAPIView(generics.RetrieveAPIView):
    serializer_class = UserDetailSerializer
    permission_classes = [IsAuthenticated]
    lookup_field = 'username'

    def get_object(self):
        username = self.kwargs['username']
        return get_object_or_404(User, username=username)


class UserUpdateAPIView(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserUpdateSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = self.queryset.filter(id=self.request.user.id)
        return queryset

    def perform_create(self, serializer):
        serializer.save(id=self.request.user.id)


class UserLogoutAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        try:
            request.user.auth_token.delete()
            return Response(status=HTTP_200_OK)
        except:
            return Response(status=HTTP_400_BAD_REQUEST)


class UserDetail(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        user = User.objects.get(id=request.user.id)
        return Response(UserDetailSerializer(user).data)


class PayUserCallBack(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        try:
            user = User.objects.get(id=request.user.id)
            user.is_paid = True
            time_now = datetime.datetime.now()
            if user.end_time <= pytz.utc.localize(time_now):
                user.end_time = datetime.datetime.now() + relativedelta(months=request.data['end_time'])
            else:
                user.end_time += relativedelta(months=request.data['end_time'])
            user.save()
        except ValueError:
            error = {
                "message": "Дещо пішло не так, зверніться в тех. підтримку!"
            }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)
        return Response({"message": 'Оплата пройшла успішно!'}, status=status.HTTP_200_OK)


class UserVerifySubs(APIView):
    def get(self, request, *args, **kwargs):
        user = User.objects.filter(
            is_paid=True, end_time__lte=datetime.datetime.now()
        )
        user.update(is_paid=False)
        return Response(status.HTTP_200_OK)


