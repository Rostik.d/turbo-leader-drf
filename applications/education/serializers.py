from rest_framework import serializers

from applications.education.models import Post, Tags


class TagsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tags
        fields = '__all__'


class EducationListSerializer(serializers.ModelSerializer):
    tags = TagsSerializer(many=True)

    class Meta:
        model = Post
        exclude = (
            'text',
        )


class EducationDetailSerializer(serializers.ModelSerializer):
    tags = TagsSerializer(many=True)

    class Meta:
        model = Post
        fields = '__all__'
