from rest_framework import routers

from applications.education.views import EductionApiView

router = routers.SimpleRouter()
router.register('', EductionApiView, basename='education')

urlpatterns = [

]

urlpatterns += router.urls
