from django.db import models


class Tags(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Тег"
        verbose_name_plural = "Теги"


class Post(models.Model):
    title = models.CharField("Заголовок", max_length=100)
    main_photo = models.ImageField(upload_to="education/photo", default="", verbose_name="Головне фото")
    text_min = models.TextField("Описание", max_length=350)
    text = models.TextField("Оснвной текст")
    tags = models.ManyToManyField(Tags, verbose_name='Теги')
    created = models.DateTimeField(auto_now_add=True)
    keywords = models.CharField("Ключовые слова", max_length=50)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Пост"
        verbose_name_plural = "Пости"
