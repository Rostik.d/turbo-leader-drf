from rest_framework.permissions import AllowAny
from rest_framework.viewsets import ReadOnlyModelViewSet

from applications.education.models import Post
from applications.education.serializers import EducationListSerializer, EducationDetailSerializer


class EductionApiView(ReadOnlyModelViewSet):
    queryset = Post.objects.all()
    permission_classes = [AllowAny]

    def get_object(self):
        pk = self.kwargs.get('pk')
        return Post.objects.get(pk=pk)

    def get_serializer_class(self):
        if self.action == 'list':
            return EducationListSerializer
        if self.action == 'retrieve':
            return EducationDetailSerializer
