from django.contrib import admin

from applications.contact.models import Contact, ContactFromLending

admin.site.register(Contact)
admin.site.register(ContactFromLending)

