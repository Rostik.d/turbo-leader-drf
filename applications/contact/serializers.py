from django.template.defaulttags import now
from rest_framework import serializers

from applications.contact.models import Contact, ContactFromLending


class ContactListSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source='user.username')
    first_name = serializers.CharField(source='user.first_name')
    last_name = serializers.CharField(source='user.last_name')
    photo = serializers.ImageField(source='user.photo')
    email = serializers.CharField(source='user.email')
    user = serializers.CharField(source='user.username')

    class Meta:
        model = Contact
        exclude = (
            'owner',
        )


class ContactUpdateSerializer(serializers.ModelSerializer):
    # comment = serializers.CharField(max_length=255)

    class Meta:
        model = Contact
        fields = (
            'comment',
        )

    def update(self, instance, validated_data):
        for field, value in validated_data.items():
            setattr(instance, field, value)

        instance.save()
        return instance


class ContactLendingListSerializer(serializers.ModelSerializer):

    class Meta:
        model = ContactFromLending
        exclude = (
            'owner',
        )


class ContactLendingSerializer(serializers.ModelSerializer):

    class Meta:
        model = ContactFromLending
        fields = "__all__"


class ContactLendingUpdateSerializer(serializers.ModelSerializer):
    # comment = serializers.CharField(max_length=255)

    class Meta:
        model = ContactFromLending
        fields = (
            'comment',
        )

    def update(self, instance, validated_data):
        for field, value in validated_data.items():
            setattr(instance, field, value)

        instance.save()
        return instance
