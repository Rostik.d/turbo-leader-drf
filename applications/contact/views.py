from rest_framework import generics
from rest_framework.mixins import ListModelMixin, CreateModelMixin
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.viewsets import ReadOnlyModelViewSet, GenericViewSet

from applications.contact.models import Contact, ContactFromLending
from applications.contact.serializers import (
    ContactListSerializer,
    ContactUpdateSerializer,
    ContactLendingUpdateSerializer,
    ContactLendingListSerializer, ContactLendingSerializer
)


class ContactApiView(ReadOnlyModelViewSet):
    queryset = Contact.objects.all()
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Contact.objects.filter(owner=self.request.user).order_by('id')

    def get_serializer_class(self):
        if self.action == 'list':
            return ContactListSerializer


class UpdateCommentView(generics.UpdateAPIView):
    serializer_class = ContactUpdateSerializer
    model = Contact
    permission_classes = [IsAuthenticated]

    def get_object(self):
        pk = self.kwargs.get('comment_id')
        return Contact.objects.get(pk=pk)


class ContactLendingApiView(
    CreateModelMixin,
    ListModelMixin,
    GenericViewSet
):
    permission_classes = [AllowAny]

    def get_queryset(self):
        return ContactFromLending.objects.filter(owner=self.request.user.username).order_by('id')

    def get_serializer_class(self):
        if self.action == 'list':
            return ContactLendingListSerializer
        if self.action == 'create':
            return ContactLendingSerializer


class UpdateLendingCommentView(generics.UpdateAPIView):
    serializer_class = ContactLendingUpdateSerializer
    model = ContactFromLending
    permission_classes = [IsAuthenticated]

    def get_object(self):
        pk = self.kwargs.get('comment_id')
        return ContactFromLending.objects.get(pk=pk)
