from django.db import models
from applications.user_profile.models import User


class Contact(models.Model):
    owner = models.ForeignKey(User, default=1, on_delete=models.CASCADE, related_name='owner_referral')
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE, related_name='referral_user')
    comment = models.CharField(max_length=150, null=True, blank=True)

    def __str__(self):
        return f'Користувач {self.user.username} є контактом: {self.owner.username}'

    class Meta:
        verbose_name = "Контакт"
        verbose_name_plural = "Контакти"


class ContactFromLending(models.Model):
    owner = models.CharField(max_length=255, verbose_name="Реферал")
    site_url = models.CharField(max_length=2000, verbose_name="URL лендінга")
    number_phone = models.CharField(max_length=150, verbose_name="Номер телефону")
    name = models.CharField(max_length=250, verbose_name="Ім'я")
    comment = models.CharField(max_length=150, null=True, blank=True, verbose_name="Коментарій")
    email = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return f'{self.name} - {self.number_phone} - {self.site_url} - {self.owner}'

    class Meta:
        verbose_name = "Контакт з лендінгу"
        verbose_name_plural = "Контакти з лендінгів"
