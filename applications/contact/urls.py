from django.urls import path
from rest_framework import routers

from applications.contact.views import (
    ContactApiView,
    UpdateCommentView,
    ContactLendingApiView,
    UpdateLendingCommentView
)

router = routers.SimpleRouter()
router.register('ref', ContactApiView, basename='contact')
router.register('lending', ContactLendingApiView, basename='lending')

urlpatterns = [
    path('<int:comment_id>/', UpdateCommentView.as_view()),
    path('lending/<int:comment_id>/', UpdateLendingCommentView.as_view()),
]

urlpatterns += router.urls
