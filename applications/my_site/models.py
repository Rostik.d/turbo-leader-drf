from django.db import models


class MySite(models.Model):
    name = models.CharField(max_length=150, null=True, blank=True)
    link = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return f'Лендінг: {self.name}'

    class Meta:
        verbose_name = "Мій сайт"
        verbose_name_plural = "Мої сайти"
