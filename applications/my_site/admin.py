from django.contrib import admin

from applications.my_site.models import MySite

admin.site.register(MySite)
