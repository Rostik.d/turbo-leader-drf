from django.conf.urls.static import static
from django.urls import path


from turboleader import settings
from . import views
from .views import (
    MySiteListView,
)

urlpatterns = [
    path('', MySiteListView.as_view()),
    path('asi-webinar', views.asi_webinar, name="asi-webinar"),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
