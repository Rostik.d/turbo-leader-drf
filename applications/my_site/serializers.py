from rest_framework import serializers

from applications.my_site.models import MySite
from applications.product.models import Product


class MySiteListSerializer(serializers.ModelSerializer):

    class Meta:
        model = MySite
        fields = (
            "name",
            "link"
        )


class ProductSerializer(serializers.ModelSerializer):
    site = MySiteListSerializer(many=True)

    class Meta:
        model = Product
        fields = (
            "id",
            "name",
            "photo",
            "main_page",
            "site"
        )
