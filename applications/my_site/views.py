from django.shortcuts import render
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from applications.my_site.serializers import ProductSerializer
from applications.product.models import Product


class MySiteListView(generics.ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = ProductSerializer

    def get_queryset(self):
        return Product.objects.all()


def asi_webinar(request):
    return render(request, 'main/asi-webinar/index.html')
