from django.apps import AppConfig


class MySiteConfig(AppConfig):
    name = 'my_site'
    verbose_name = "Мої сайти"
