from django.db import models

from applications.my_site.models import MySite


class Product(models.Model):
    name = models.CharField(max_length=200)
    photo = models.ImageField(upload_to='product_pic', blank=True)
    main_page = models.URLField(blank=True, null=True)
    site = models.ManyToManyField(MySite)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Продукт"
        verbose_name_plural = "Продукти"
